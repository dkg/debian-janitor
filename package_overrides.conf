# vim: ft=proto
# Ideally all packages should have the Repository field set in debian/upstream/metadata.
# In lieu of that, the upstream location can be set here.

package {
   name: "basket"
   upstream_branch_url: "https://github.com/kelvie/basket"
}
package {
  name: "apache2"
  upstream_branch_url: "http://svn.apache.org/repos/asf/httpd/httpd/trunk"
}
package {
  name: "lilypond"
  upstream_branch_url: "git://git.sv.gnu.org/lilypond"
}
package {
  name: "gnome-calculator"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-calculator"
}
package {
  name: "reprepro"
  upstream_branch_url: "git://git.debian.org/mirrorer/reprepro.git"
}
package {
  name: "gnome-mines"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-mines"
}
package {
  name: "pulseaudio"
  upstream_branch_url: "git://anongit.freedesktop.org/pulseaudio/pulseaudio"
}
package {
  name: "x264"
  upstream_branch_url: "git://git.videolan.org/x264.git"
}
package {
  name: "ant"
  upstream_branch_url: "http://svn.apache.org/repos/asf/ant/core/trunk/"
}
package {
  name: "haproxy"
  upstream_branch_url: "lp:haproxy"
}
package {
  name: "evolution"
  upstream_branch_url: "git://git.gnome.org/evolution"
}
package {
  name: "lua-lpeg"
  upstream_branch_url: "https://github.com/daurnimator/lpeg_patterns"
}
package {
  name: "optipng"
  upstream_branch_url: "lp:optipng"
}
package {
  name: "metacity"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/metacity.git"
}
package {
  name: "hexchat"
  upstream_branch_url: "https://github.com/TingPing/hexchat-snap.git"
}
package {
  name: "gnome-themes-extra"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-themes-standard"
}
package {
  name: "gtk2-engines"
  upstream_branch_url: "http://svn.gnome.org/svn/gtk-engines/trunk"
}
package {
  name: "i3lock"
  upstream_branch_url: "https://github.com/codejamninja/i3lock-color-ubuntu"
}
package {
  name: "nautilus"
  upstream_branch_url: "git://git.gnome.org/nautilus.git"
}
package {
  name: "ndisc6"
  upstream_branch_url: "http://git.remlab.net/git/ndisc6.git"
}
package {
  name: "blender"
  upstream_branch_url: "https://svn.blender.org/svnroot/bf-blender/trunk/blender"
}
package {
  name: "four-in-a-row"
  upstream_branch_url: "https://git.gnome.org/browse/four-in-a-row"
}
package {
  name: "swell-foop"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/swell-foop.git"
}
package {
  name: "gcc-7"
  upstream_branch_url: "svn://gcc.gnu.org/svn/gcc/trunk"
}
package {
  name: "alsa-oss"
  upstream_branch_url: "git://git.alsa-project.org/alsa-plugins.git"
}
package {
  name: "gmpc"
  upstream_branch_url: "git://repo.or.cz/gmpc.git"
}
package {
  name: "gnome-color-manager"
  upstream_branch_url: "git://git.gnome.org/gnome-color-manager"
}
package {
  name: "notification-daemon"
  upstream_branch_url: "git://git.gnome.org/notification-daemon"
}
package {
  name: "gnome-sushi"
  upstream_branch_url: "git://git.gnome.org/sushi"
}
package {
  name: "devhelp"
  upstream_branch_url: "git://git.gnome.org/devhelp"
}
package {
  name: "acct"
  upstream_branch_url: "http://svn.savannah.gnu.org/svn/acct/"
}
package {
  name: "bsdgames"
  upstream_branch_url: "git://github.com/quasipedia/atc-ng.git"
}
package {
  name: "picard"
  upstream_branch_url: "https://github.com/metabrainz/picard.git"
}
package {
  name: "calibre"
  upstream_branch_url: "lp:calibre"
}
package {
  name: "file-roller"
  upstream_branch_url: "git://git.gnome.org/file-roller"
}
package {
  name: "ardour"
  upstream_branch_url: "git://git.ardour.org/ardour/ardour.git"
}
package {
  name: "rtmpdump"
  upstream_branch_url: "lp:rtmpdump"
}
package {
  name: "asterisk"
  upstream_branch_url: "http://svn.asterisk.org/svn/asterisk/trunk"
}
package {
  name: "lighttpd"
  upstream_branch_url: "svn://svn.lighttpd.net/lighttpd/trunk-cscvs"
}
package {
  name: "bleachbit"
  upstream_branch_url: "https://github.com/az0/bleachbit.git"
}
package {
  name: "alpine"
  upstream_branch_url: "https://svn.cac.washington.edu/public/alpine/snapshots"
}
package {
  name: "beautifulsoup4"
  upstream_branch_url: "lp:beautifulsoup"
}
package {
  name: "wayland-protocols"
  upstream_branch_url: "git://anongit.freedesktop.org/wayland/wayland-protocols"
}
package {
  name: "python-dateutil"
  upstream_branch_url: "lp:dateutil"
}
package {
  name: "supervisor"
  upstream_branch_url: "http://svn.supervisord.org/supervisor/trunk"
}
package {
  name: "gnome-doc-utils"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-doc-utils/trunk"
}
package {
  name: "five-or-more"
  upstream_branch_url: "https://git.gnome.org/browse/five-or-more"
}
package {
  name: "wireless-regdb"
  upstream_branch_url: "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
}
package {
  name: "wmaker"
  upstream_branch_url: "git://repo.or.cz/wmaker-crm.git,branch=next"
}
package {
  name: "plymouth"
  upstream_branch_url: "git://anongit.freedesktop.org/plymouth"
}
package {
  name: "extundelete"
  upstream_branch_url: "git://extundelete.git.sourceforge.net/gitroot/extundelete/extundelete"
}
package {
  name: "directfb"
  upstream_branch_url: "git://git.directfb.org/git/directfb/core/DirectFB.git"
}
package {
  name: "gnome-backgrounds"
  upstream_branch_url: "http://svn.gwendallebihan.net/gnome3-background-slideshow/trunk"
}
package {
  name: "gnome-flashback"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-flashback.git"
}
package {
  name: "ffmpeg"
  upstream_branch_url: "git://git.videolan.org/ffmpeg.git"
}
package {
  name: "gnome-klotski"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-klotski"
}
package {
  name: "mupdf"
  upstream_branch_url: "git://git.ghostscript.com/mupdf.git"
}
package {
  name: "qrencode"
  upstream_branch_url: "lp:qr-code-creator"
}
package {
  name: "policykit-1-gnome"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/policykit-gnome.git"
}
package {
  name: "command-not-found"
  upstream_branch_url: "lp:command-not-found"
}
package {
  name: "roundcube"
  upstream_branch_url: "https://svn.roundcube.net/trunk/roundcubemail"
}
package {
  name: "cdparanoia"
  upstream_branch_url: "http://svn.xiph.org/trunk/cdparanoia"
}
package {
  name: "gpm"
  upstream_branch_url: "git://git.schottelius.org/gpm"
}
package {
  name: "cheese"
  upstream_branch_url: "git://git.gnome.org/cheese"
}
package {
  name: "chrome-gnome-shell"
  upstream_branch_url: "https://git.gnome.org/browse/chrome-gnome-shell"
}
package {
  name: "gpsd"
  upstream_branch_url: "git://git.savannah.nongnu.org/gpsd.git"
}
package {
  name: "privoxy"
  upstream_branch_url: "lp:guadalinexedu-content-control"
}
package {
  name: "bluez"
  upstream_branch_url: "https://kernel.googlesource.com/pub/scm/bluetooth/bluez.git"
}
package {
  name: "grep"
  upstream_branch_url: "git://git.sv.gnu.org/grep.git"
}
package {
  name: "libxml2"
  upstream_branch_url: "http://svn.gnome.org/svn/libxml2/trunk"
}
package {
  name: "lightsoff"
  upstream_branch_url: "https://git.gnome.org/browse/lightsoff"
}
package {
  name: "alsa-utils"
  upstream_branch_url: "git://git.alsa-project.org/alsa-utils.git"
}
package {
  name: "gdb"
  upstream_branch_url: "git://sourceware.org/git/gdb.git"
}
package {
  name: "gedit"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gedit.git"
}
package {
  name: "rsync"
  upstream_branch_url: "git://git.samba.org/rsync.git"
}
package {
  name: "gnome-tweaks"
  upstream_branch_url: "git://git.gnome.org/gnome-tweak-tool"
}
package {
  name: "rhythmbox"
  upstream_branch_url: "git://git.gnome.org/rhythmbox"
}
package {
  name: "ristretto"
  upstream_branch_url: "git://git.xfce.org/apps/ristretto"
}
package {
  name: "network-manager-vpnc"
  upstream_branch_url: "git://git.gnome.org/network-manager-vpnc"
}
package {
  name: "geogebra"
  upstream_branch_url: "lp:geogebra"
}
package {
  name: "asymptote"
  upstream_branch_url: "https://asymptote.svn.sourceforge.net/svnroot/asymptote/trunk/asymptote"
}
package {
  name: "thunar"
  upstream_branch_url: "git://git.xfce.org/xfce/thunar"
}
package {
  name: "putty"
  upstream_branch_url: "svn://svn.tartarus.org/sgt/putty"
}
package {
  name: "pvm"
  upstream_branch_url: "lp:pvm-or"
}
package {
  name: "pybtex"
  upstream_branch_url: "lp:pybtex"
}
package {
  name: "at-spi2-core"
  upstream_branch_url: "git://git.gnome.org/at-spi"
}
package {
  name: "totem"
  upstream_branch_url: "git://git.gnome.org/totem.git"
}
package {
  name: "dune-common"
  upstream_branch_url: "https://gitlab.dune-project.org/core/dune-common"
}
package {
  name: "fonts-cantarell"
  upstream_branch_url: "https://git.gnome.org/browse/cantarell-fonts"
}
package {
  name: "intel-gpu-tools"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/driver/xf86-video-intel"
}
package {
  name: "nose"
  upstream_branch_url: "git://github.com/nose-devs/nose.git"
}
package {
  name: "postfix"
  upstream_branch_url: "https://git.launchpad.net/postfix"
}
package {
  name: "meld"
  upstream_branch_url: "git://git.gnome.org/meld"
}
package {
  name: "pypy"
  upstream_branch_url: "lp:pypy"
}
package {
  name: "geany"
  upstream_branch_url: "git://github.com/geany/geany"
}
package {
  name: "evolution-data-server"
  upstream_branch_url: "git://git.gnome.org/evolution-data-server"
}
package {
  name: "tumbler"
  upstream_branch_url: "git://git.xfce.org/apps/tumbler"
}
package {
  name: "dconf-editor"
  upstream_branch_url: "https://git.gnome.org/browse/dconf-editor"
}
package {
  name: "junit"
  upstream_branch_url: "git://github.com/KentBeck/junit.git"
}
package {
  name: "gnulib"
  upstream_branch_url: "git://git.sv.gnu.org/gnulib.git"
}
package {
  name: "gnome-software"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-software"
}
package {
  name: "unbound"
  upstream_branch_url: "https://nlnetlabs.nl/svn/unbound/"
}
package {
  name: "unittest2"
  upstream_branch_url: "http://hg.python.org/unittest2"
}
package {
  name: "mutt"
  upstream_branch_url: "http://dev.mutt.org/hg/mutt"
}
package {
  name: "mutter"
  upstream_branch_url: "https://git.gnome.org/browse/mutter/"
}
package {
  name: "geany-plugins"
  upstream_branch_url: "git://github.com/geany/geany"
}
package {
  name: "m17n-db"
  upstream_branch_url: "git://github.com/avro-phonetic/Avro-Phonetic-m17n.git"
}
package {
  name: "glade"
  upstream_branch_url: "git://git.gnome.org/glade/"
}
package {
  name: "gnome-panel"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-panel.git"
}
package {
  name: "network-manager-openconnect"
  upstream_branch_url: "git://git.gnome.org/network-manager-openconnect"
}
package {
  name: "nano"
  upstream_branch_url: "git://git.sv.gnu.org/nano.git"
}
package {
  name: "dosbox"
  upstream_branch_url: "http://svn.code.sf.net/p/dosbox/code-0/dosbox/trunk/"
}
package {
  name: "mpd"
  upstream_branch_url: "https://svn.musicpd.org/mpd/trunk"
}
package {
  name: "gnome-applets"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-applets.git"
}
package {
  name: "gnome-bluetooth"
  upstream_branch_url: "git://git.gnome.org/gnome-bluetooth"
}
package {
  name: "gnome-boxes"
  upstream_branch_url: "git://git.gnome.org/gnome-boxes"
}
package {
  name: "gnome-clocks"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-clocks.git"
}
package {
  name: "gnome-common"
  upstream_branch_url: "git://git.gnome.org/gnome-common"
}
package {
  name: "gnome-documents"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-documents"
}
package {
  name: "alacarte"
  upstream_branch_url: "http://svn.gnome.org/svn/alacarte/trunk"
}
package {
  name: "gnome-icon-theme"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-icon-theme/trunk"
}
package {
  name: "mpich"
  upstream_branch_url: "https://svn.mcs.anl.gov/repos/mpi/mpich2/trunk"
}
package {
  name: "gnome-mahjongg"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-mahjongg"
}
package {
  name: "gnome-music"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-music"
}
package {
  name: "gnome-nettool"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-nettool/trunk"
}
package {
  name: "gnome-screensaver"
  upstream_branch_url: "git://git.gnome.org/gnome-screensaver"
}
package {
  name: "gnome-session"
  upstream_branch_url: "lp:gnome-session-shutdown"
}
package {
  name: "network-manager-openvpn"
  upstream_branch_url: "git://git.gnome.org/network-manager-openvpn"
}
package {
  name: "awffull"
  upstream_branch_url: "lp:awffull"
}
package {
  name: "gnome-system-log"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-system-log"
}
package {
  name: "gnome-tetravex"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-tetravex.git"
}
package {
  name: "gnome-todo"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-todo"
}
package {
  name: "gnome-user-docs"
  upstream_branch_url: "git://git.gnome.org/gnome-user-docs"
}
package {
  name: "gnote"
  upstream_branch_url: "git://git.gnome.org/gnote"
}
package {
  name: "gnupg2"
  upstream_branch_url: "svn://cvs.gnupg.org/gnupg/branches/STABLE-BRANCH-2-0"
}
package {
  name: "gobject-introspection"
  upstream_branch_url: "git://git.gnome.org/gobject-introspection.git"
}
package {
  name: "gcc-arm-none-eabi"
  upstream_branch_url: "svn://gcc.gnu.org/svn/gcc/trunk"
}
package {
  name: "evolver"
  upstream_branch_url: "lp:evolver"
}
package {
  name: "monitoring-plugins"
  upstream_branch_url: "lp:percona-monitoring-plugins"
}
package {
  name: "ffmpeg2theora"
  upstream_branch_url: "git://git.xiph.org/ffmpeg2theora.git"
}
package {
  name: "libtool"
  upstream_branch_url: "git://git.sv.gnu.org/libtool.git"
}
package {
  name: "openvpn"
  upstream_branch_url: "https://github.com/OpenVPN/openvpn.git"
}
package {
  name: "python-numpy"
  upstream_branch_url: "git://github.com/numpy/numpy.git"
}
package {
  name: "orage"
  upstream_branch_url: "git://git.xfce.org/apps/orage"
}
package {
  name: "iptables"
  upstream_branch_url: "git://git.netfilter.org/iptables"
}
package {
  name: "gnome-terminal"
  upstream_branch_url: "git://git.gnome.org/gnome-terminal"
}
package {
  name: "gnome-weather"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-weather"
}
package {
  name: "groovy"
  upstream_branch_url: "git://git.codehaus.org/groovy-git.git"
}
package {
  name: "gucharmap"
  upstream_branch_url: "https://git.gnome.org/browse/gucharmap"
}
package {
  name: "kmod"
  upstream_branch_url: "git://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git"
}
package {
  name: "guvcview"
  upstream_branch_url: "git://git.code.sf.net/p/guvcview/git-master"
}
package {
  name: "ivy"
  upstream_branch_url: "lp:ivy"
}
package {
  name: "gvfs"
  upstream_branch_url: "git://git.gnome.org/gvfs"
}
package {
  name: "gzip"
  upstream_branch_url: "git://git.sv.gnu.org/gzip.git"
}
package {
  name: "sipcalc"
  upstream_branch_url: "lp:sipcalc"
}
package {
  name: "caribou"
  upstream_branch_url: "https://git.gnome.org/browse/caribou"
}
package {
  name: "evince"
  upstream_branch_url: "git://git.gnome.org/evince.git"
}
package {
  name: "poppler-data"
  upstream_branch_url: "git://anongit.freedesktop.org/poppler/poppler"
}
package {
  name: "gnome-calendar"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-calendar"
}
package {
  name: "dbus"
  upstream_branch_url: "git://anongit.freedesktop.org/dbus/dbus"
}
package {
  name: "gnome-maps"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-maps"
}
package {
  name: "gnome-sound-recorder"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/gnome-sound-recorder.git"
}
package {
  name: "gnome-system-monitor"
  upstream_branch_url: "http://svn.gnome.org/svn/libgtop/trunk"
}
package {
  name: "python-pkginfo"
  upstream_branch_url: "lp:pkginfo"
}
package {
  name: "lyx"
  upstream_branch_url: "svn://svn.lyx.org/lyx/lyx-devel/trunk"
}
package {
  name: "paprefs"
  upstream_branch_url: "git://anongit.freedesktop.org/pulseaudio/pulseaudio"
}
package {
  name: "libapache2-mod-fcgid"
  upstream_branch_url: "http://svn.apache.org/repos/asf/httpd/mod_fcgid/trunk/"
}
package {
  name: "parallel"
  upstream_branch_url: "https://git.savannah.gnu.org/git/parallel.git"
}
package {
  name: "parole"
  upstream_branch_url: "git://git.xfce.org/apps/parole"
}
package {
  name: "mpc"
  upstream_branch_url: "http://mpc-hc.svn.sourceforge.net/svnroot/mpc-hc/trunk"
}
package {
  name: "network-manager"
  upstream_branch_url: "https://git.launchpad.net/network-manager"
}
package {
  name: "network-manager-pptp"
  upstream_branch_url: "git://git.gnome.org/network-manager-pptp"
}
package {
  name: "gnome-disk-utility"
  upstream_branch_url: "git://git.gnome.org/gnome-disk-utility"
}
package {
  name: "gnome-menus"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-menus/trunk"
}
package {
  name: "gnome-settings-daemon"
  upstream_branch_url: "git://git.gnome.org/gnome-settings-daemon"
}
package {
  name: "gnome-shell"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-shell/"
}
package {
  name: "mousepad"
  upstream_branch_url: "git://git.xfce.org/apps/mousepad"
}
package {
  name: "adwaita-icon-theme"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-icon-theme/trunk"
}
package {
  name: "sysvinit"
  upstream_branch_url: "svn://svn.savannah.gnu.org/sysvinit/sysvinit/trunk/"
}
package {
  name: "libcloud"
  upstream_branch_url: "https://svn.apache.org/repos/asf/libcloud/trunk"
}
package {
  name: "playonlinux"
  upstream_branch_url: "git://github.com/PlayOnLinux/POL-POM-4.git"
}
package {
  name: "apper"
  upstream_branch_url: "git://anongit.kde.org/apper"
}
package {
  name: "ktorrent"
  upstream_branch_url: "git://anongit.kde.org/ktorrent"
}
package {
  name: "hitori"
  upstream_branch_url: "https://git.gnome.org/browse/hitori"
}
package {
  name: "tali"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/tali.git"
}
package {
  name: "joystick"
  upstream_branch_url: "git://git.code.sf.net/p/linuxconsole/code"
}
package {
  name: "tar"
  upstream_branch_url: "git://git.sv.gnu.org/tar.git/"
}
package {
  name: "bijiben"
  upstream_branch_url: "https://git.gnome.org/browse/bijiben"
}
package {
  name: "ldaptor"
  upstream_branch_url: "svn://inoi.fi/ldaptor/trunk"
}
package {
  name: "mercurial"
  upstream_branch_url: "http://selenic.com/repo/hg"
}
package {
  name: "qd"
  upstream_branch_url: "https://freeqda.svn.sourceforge.net/svnroot/freeqda/trunk"
}
package {
  name: "qbittorrent"
  upstream_branch_url: "https://github.com/qbittorrent/qBittorrent.git"
}
package {
  name: "busybox"
  upstream_branch_url: "git://busybox.net/busybox.git"
}
package {
  name: "gnome-getting-started-docs"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-getting-started-docs"
}
package {
  name: "gnome-keyring"
  upstream_branch_url: "git://git.gnome.org/gnome-keyring"
}
package {
  name: "quadrapassel"
  upstream_branch_url: "https://gitlab.gnome.org/GNOME/quadrapassel.git"
}
package {
  name: "r-base"
  upstream_branch_url: "http://svn.r-project.org/R/trunk"
}
package {
  name: "binfmt-support"
  upstream_branch_url: "git://git.sv.gnu.org/binfmt-support.git"
}
package {
  name: "rcs"
  upstream_branch_url: "git://git.savannah.gnu.org/rcs.git"
}
package {
  name: "mgdiff"
  upstream_branch_url: "git://github.com/mgedmin/imgdiff.git"
}
package {
  name: "gcc-8"
  upstream_branch_url: "svn://gcc.gnu.org/svn/gcc/branches/gcc-4_7-branch"
}
package {
  name: "filezilla"
  upstream_branch_url: "http://filezilla.svn.sourceforge.net/svnroot/filezilla/FileZilla3/trunk"
}
package {
  name: "tcpdf"
  upstream_branch_url: "https://salsa.debian.org/phpmyadmin-team/tcpdf.git"
}
package {
  name: "gedit-plugins"
  upstream_branch_url: "git://git.gnome.org/gedit-plugins"
}
package {
  name: "geoclue-2.0"
  upstream_branch_url: "git://anongit.freedesktop.org/geoclue"
}
package {
  name: "epiphany-browser"
  upstream_branch_url: "git://git.gnome.org/epiphany"
}
package {
  name: "nginx"
  upstream_branch_url: "svn://svn.nginx.org/nginx/trunk/"
}
package {
  name: "sbcl"
  upstream_branch_url: "git://git.code.sf.net/p/sbcl/sbcl"
}
package {
  name: "scons"
  upstream_branch_url: "https://bitbucket.org/scons/scons"
}
package {
  name: "scribus"
  upstream_branch_url: "svn://scribus.net/trunk/Scribus"
}
package {
  name: "seahorse"
  upstream_branch_url: "http://svn.gnome.org/svn/seahorse/trunk"
}
package {
  name: "simplejson"
  upstream_branch_url: "http://simplejson.googlecode.com/svn/trunk"
}
package {
  name: "dillo"
  upstream_branch_url: "http://hg.dillo.org/dillo"
}
package {
  name: "sound-juicer"
  upstream_branch_url: "http://svn.gnome.org/svn/sound-juicer/trunk"
}
package {
  name: "speex"
  upstream_branch_url: "http://svn.xiph.org/trunk/speex"
}
package {
  name: "squid"
  upstream_branch_url: "https://github.com/squid-cache/squid.git"
}
package {
  name: "sssd"
  upstream_branch_url: "git://git.fedorahosted.org/git/sssd.git"
}
package {
  name: "strace"
  upstream_branch_url: "git://git.code.sf.net/p/strace/code"
}
package {
  name: "brasero"
  upstream_branch_url: "git://git.gnome.org/brasero.git"
}
package {
  name: "strongswan"
  upstream_branch_url: "git://git.strongswan.org/strongswan.git"
}
package {
  name: "pspp"
  upstream_branch_url: "git://git.sv.gnu.org/pspp.git"
}
package {
  name: "vino"
  upstream_branch_url: "http://svn.gnome.org/svn/vino/trunk"
}
package {
  name: "virtualbox"
  upstream_branch_url: "http://www.virtualbox.org/svn/vbox/trunk"
}
package {
  name: "virtualbox-guest-additions-iso"
  upstream_branch_url: "http://www.virtualbox.org/svn/vbox/trunk"
}
package {
  name: "vpnc-scripts"
  upstream_branch_url: "lp:vpnc"
}
package {
  name: "xdg-user-dirs"
  upstream_branch_url: "git://anongit.freedesktop.org/xdg/xdg-user-dirs"
}
package {
  name: "xdg-user-dirs-gtk"
  upstream_branch_url: "git://anongit.freedesktop.org/xdg/xdg-user-dirs"
}
package {
  name: "xdg-utils"
  upstream_branch_url: "git://anongit.freedesktop.org/xdg/xdg-utils"
}
package {
  name: "xfce4-appfinder"
  upstream_branch_url: "git://git.xfce.org/xfce/xfce4-appfinder"
}
package {
  name: "xfce4-pulseaudio-plugin"
  upstream_branch_url: "git://git.xfce.org/panel-plugins/xfce4-pulseaudio-plugin"
}
package {
  name: "xfce4-terminal"
  upstream_branch_url: "git://git.xfce.org/apps/xfce4-terminal"
}
package {
  name: "xfce4-whiskermenu-plugin"
  upstream_branch_url: "git://git.xfce.org/panel-plugins/xfce4-whiskermenu-plugin"
}
package {
  name: "xfdesktop4"
  upstream_branch_url: "git://git.xfce.org/xfce/xfdesktop"
}
package {
  name: "xfconf"
  upstream_branch_url: "git://git.xfce.org/xfce/xfconf"
}
package {
  name: "xfwm4"
  upstream_branch_url: "git://git.xfce.org/xfce/xfwm4"
}
package {
  name: "xml2"
  upstream_branch_url: "http://svn.gnome.org/svn/libxml2/trunk"
}
package {
  name: "xserver-xorg-video-amdgpu"
  upstream_branch_url: "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
}
package {
  name: "xserver-xorg-video-ati"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/driver/xf86-video-ati"
}
package {
  name: "xserver-xorg-video-intel"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/driver/xf86-video-intel"
}
package {
  name: "xserver-xorg-input-evdev"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/driver/xf86-input-evdev"
}
package {
  name: "xserver-xorg-video-savage"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/xserver"
}
package {
  name: "xserver-xorg-video-vesa"
  upstream_branch_url: "git://anongit.freedesktop.org/xorg/xserver"
}
package {
  name: "yelp-xsl"
  upstream_branch_url: "http://svn.gnome.org/svn/gnome-doc-utils/trunk"
}
package {
  name: "yelp"
  upstream_branch_url: "lp:unity-scope-yelp"
}
package {
  name: "zim"
  upstream_branch_url: "https://github.com/jaap-karssenberg/zim-desktop-wiki.git"
}
package {
  name: "zynaddsubfx"
  upstream_branch_url: "git://zynaddsubfx.git.sourceforge.net/gitroot/zynaddsubfx/zynaddsubfx"
}
package {
  name: "nut"
  upstream_branch_url: "https://github.com/networkupstools/nut.git"
}
package {
  name: "ffc"
  upstream_branch_url: "lp:ffc"
}
package {
  name: "mplayer"
  upstream_branch_url: "svn://svn.mplayerhq.hu/mplayer/trunk"
}
package {
  name: "gnome-control-center"
  upstream_branch_url: "git://git.gnome.org/gnome-control-center"
}
package {
  name: "pygtkspellcheck"
  upstream_branch_url: "git://github.com/koehlma/pygtkspellcheck.git"
}
package {
  name: "texworks"
  upstream_branch_url: "http://texworks.googlecode.com/svn/trunk/"
}
package {
  name: "amavisd-new"
  upstream_branch_url: "http://mirrors.catpipe.net/amavisd-new/hgweb/amavis/"
}
package {
  name: "cpuid"
  upstream_branch_url: "http://dev.ezix.org/source/packages/lshw/development"
}
package {
  name: "handbrake"
  upstream_branch_url: "svn://svn.handbrake.fr/HandBrake/trunk"
}
package {
  name: "tree"
  upstream_branch_url: "git://git.code.sf.net/p/treegenerator/code"
}
package {
  name: "vinagre"
  upstream_branch_url: "git://git.gnome.org/vinagre"
}
package {
  name: "quilt"
  upstream_branch_url: "git://git.savannah.nongnu.org/quilt.git"
}
package {
  name: "gimp"
  upstream_branch_url: "git://git.gnome.org/gimp"
}
package {
  name: "vpnc"
  upstream_branch_url: "lp:vpnc"
}
package {
  name: "gradle"
  upstream_branch_url: "git://github.com/gradle/gradle"
}
package {
  name: "tor"
  upstream_branch_url: "https://git.torproject.org/tor.git"
}
package {
  name: "gnome-dictionary"
  upstream_branch_url: "https://git.gnome.org/browse/gnome-dictionary"
}
package {
  name: "stress"
  upstream_branch_url: "git://kernel.ubuntu.com/cking/stress-ng"
}
package {
  name: "sed"
  upstream_branch_url: "git://git.sv.gnu.org/sed.git"
}
package {
  name: "colord"
  upstream_branch_url: "git://gitorious.org/colord/colord-gtk.git"
}
package {
  name: "visp"
  upstream_branch_url: "svn://scm.gforge.inria.fr/svn/visp/trunk/ViSP"
}
package {
  name: "xfce4-power-manager"
  upstream_branch_url: "git://git.xfce.org/xfce/xfce4-power-manager"
}
package {
  name: "xfce4-session"
  upstream_branch_url: "git://git.xfce.org/xfce/xfce4-session"
}
package {
  name: "xfce4-settings"
  upstream_branch_url: "git://git.xfce.org/xfce/xfce4-settings"
}
package {
  name: "curl"
  upstream_branch_url: "git://github.com/bagder/curl.git"
}
package {
  name: "cup"
  upstream_branch_url: "http://www2.cs.tum.edu/repos/cup/develop/"
}
package {
  name: "chromium"
  upstream_branch_url: "https://chromium.googlesource.com/chromium/src"
}
package {
  name: "mc"
  upstream_branch_url: "https://github.com/MidnightCommander/mc.git"
}
package {
  name: "python3.7"
  upstream_branch_url: "https://github.com/python/cpython -b 3.7"
}
package {
  name: "python3.8"
  upstream_branch_url: "https://github.com/python/cpython -b 3.8"
}
package {
  name: "cups-filters"
  upstream_branch_url: "https://github.com/OpenPrinting/cups-filters"
}
package {
  name: "libgcrypt"
  upstream_branch_url: "git://git.gnupg.org/libgcrypt.git"
}
package {
  name: "lua-cqueues"
  upstream_branch_url: "http://25thandClement.com/~william/projects/cqueues.git"
}
package {
  name: "raidutils"
  upstream_branch_url: "http://anonymous:@i2o.shadowconnect.com/svn/repos/raidutils/trunk"
}
package {
  name: "pgbouncer"
  upstream_branch_url: "https://github.com/pgbouncer/pgbouncer"
}
package {
  name: "xauth"
  # packaging_branch_url: "https://salsa.debian.org/xorg-team/app/xauth"
}
